<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sanbercode Hari 1</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label for="firstName">First name :</label> <br> <br>
        <input type="text" name="firstName"> <br> <br>

        <label for="lastName">Last name</label> <br> <br>
        <input type="text" name="lastName"> <br> <br>

        <label for="gender">Gender</label> <br> <br>
        <input type="radio" name="radio" id="" value="Male">
        <label for="">Male</label> <br>
        <input type="radio" name="radio" id="" value="Female">
        <label for="">Female</label> <br>
        <input type="radio" name="radio" id="" value="Other">
        <label for="">Other</label> <br> <br>

        <label for="">Nationality:</label> <br> <br>
        <select name="nationality" id="">
            <option value="">Indonesian</option>
            <option value="">Singaphoran</option>
            <option value="">Malaysian</option>
            <option value="">Australian</option>
        </select> <br> <br>

        <label for="">Language Spoken:</label> <br> <br>
        <input type="checkbox" name="spoke">
        <label for="">Bahasa Indonesia</label> <br>
        <input type="checkbox" name="spoke"> 
        <label for="">English</label> <br>
        <input type="checkbox" name="spoke">
        <label for="">Arabic</label> <br>
        <input type="checkbox" name="spoke">
        <label for="">Japanese</label> <br> <br>


        <label for="">Bio:</label> <br> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>

        <button type="submit">Sign Up</button>
    </form>
</body>
</html>